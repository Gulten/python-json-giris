import json
import os

class User:
    def __init__(self, username, password, email):
        self.username = username
        self.password = password
        self.email = email

class UserRepository:
    def __init__(self):
        self.users = []
        self.isLoggedIn = False
        self.currentUser = {}

        self.loadUser()

    def loadUser(self):
        if os.path.exists('users.json'):
            with open('users.json', 'r', encoding='utf-8') as file:
                users = json.load(file)
                for user in users:
                    user = json.loads(user)
                    newUser = User(username = user['username'], password = user['password'], email = user['email'])
                    self.users.append(newUser)
            print(self.users)        

    def register(self, user: User):
        self.users.append(user)
        self.savetoFile()
        print("User created")

    def login(self, username, password):
        if self.isLoggedIn:
            print('You are already logged in')
        else:    
            for user in self.users:
                if user.username == username and user.password == password:
                    self.isLoggedIn = True
                    self.currentUser = user
                    print('Login successful')
                    break

    def logout(self):
        self.isLoggedIn = False
        self.currentUser = {}   
        print('Exit successful')

    def identity(self):
        if self.isLoggedIn:
            print(f'username: {self.currentUser.username}')
        else:
            print('You are not logged in')

    def savetoFile(self):
        list = []

        for user in self.users:
            list.append(json.dumps(user.__dict__))

        with open('users.json', 'w') as file:
            json.dump(list, file)


repository = UserRepository()
while True:
    print('Menu' . center(50, '*'))
    select = input('1- Register\n2- Login\n3- Logout\n4- Identity\n5- Exit\nYour Selection:')

    if select == '5':
        break
    else:
        if select == '1':
            # register
            username = input('Username: ')
            password = input('Password: ')
            email = input('Email: ')

            user = User(username=username, password=password, email=email)
            repository.register(user)

            print(repository.users)



        elif select == '2':
            # login
            if repository.isLoggedIn:
                print('You are already logged in')
            else:    
                username = input('Username: ')
                password = input('Password: ')
                repository.login(username, password)

        elif select == '3':
            # logout  
            repository.logout()

        elif select == '4':
            # identity 
            repository.identity()

        elif select == '5':
            pass  # exit

        else:
            print('Wrong selection')


